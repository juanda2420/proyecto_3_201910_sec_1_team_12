package logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.json.simple.parser.ParseException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import data_structures.IQueue;
import data_structures.LinearProbingHash;
import data_structures.Queue;
import vo.Arcos_Calles;
import vo.InterseccionMallaVial;
import vo.VOMovingViolations;

public class MovingViolationsManager {

	Queue<InterseccionMallaVial> vertices = new Queue<InterseccionMallaVial>();

	Queue<Arcos_Calles> arcos = new Queue<Arcos_Calles>();

	LinearProbingHash<String,VOMovingViolations> tabla = new LinearProbingHash<String, VOMovingViolations>();

	int contador = 0;

	public MovingViolationsManager() {

	}

	public void cargarJSON_Pequeno(String pRuta) throws IOException, ParseException {

		FileInputStream fileInputStream = null;
		String data = "";
		StringBuffer stringBuffer = new StringBuffer("");
		try {
			fileInputStream = new FileInputStream(pRuta);
			int i;
			while ((i = fileInputStream.read()) != -1) {
				stringBuffer.append((char) i);
			}
			data = stringBuffer.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileInputStream != null) {
				fileInputStream.close();
			}
		}

		// PRIMERA PARTE : VERTICES

		String[] grande = data.split("],");

		String vertices2 = grande[0];

		String arcos2 = grande[1];

		vertices2 = vertices2.replace("[", "@");

		vertices2 = vertices2.split("@")[1];

		String[] datosVertice = vertices2.split(",");

		for (int i = 0; i < datosVertice.length; i++) {

			String lineaActual = datosVertice[i];

			lineaActual = lineaActual.replace("{\"", "");
			lineaActual = lineaActual.replace("\"}", "");
			lineaActual = lineaActual.replace("\":\"", "");
			String[] datosMasPequenos = lineaActual.split(";");

			String identificador = datosMasPequenos[0].split(":")[1];

			String latitud = datosMasPequenos[1].split(":")[1];

			String longitud = datosMasPequenos[2].split(":")[1];

			InterseccionMallaVial nuevo = new InterseccionMallaVial(identificador, latitud, longitud);
			vertices.enqueue(nuevo);

		}

		// SEGUNA PARTE : ARCOS

		arcos2 = arcos2.replace("]}", "");
		arcos2 = arcos2.replace("Arcos\":[", "");

		String[] datosArcos = arcos2.split(",");

		for (int i = 0; i < datosArcos.length; i++) {

			String lineaActual = datosArcos[i];

			lineaActual = lineaActual.replace("{\"", "");
			lineaActual = lineaActual.replace("\"}", "");
			lineaActual = lineaActual.replace("\":\"", "");

			String[] datosMasPequenos = lineaActual.split(";");

			String identificador = datosMasPequenos[0].split(":")[1];

			String otrosDatos = datosMasPequenos[1].replace("vertices : [", "");
			otrosDatos = otrosDatos.replace("]", "");

			String[] paraArreglo = otrosDatos.split("-");

			LinkedList<String> delActual = new LinkedList<>();

			for (int j = 0; j < paraArreglo.length; j++) {
				delActual.add(paraArreglo[j]);
			}

			Arcos_Calles nuevoArco = new Arcos_Calles(identificador, delActual);

			arcos.enqueue(nuevoArco);

		}

	}

	@SuppressWarnings("unchecked")
	public void cargarJSON_Grande(String pRuta) throws IOException {

		JsonParser jsonParser = new JsonParser();

		try (FileReader reader = new FileReader(pRuta)) {

			Object obj = jsonParser.parse(reader);

			JsonArray arreglo = (JsonArray) obj;

			for (JsonElement actual : arreglo) {

				JsonObject nuevo = actual.getAsJsonObject();

				String identificador_actual = nuevo.get("id").getAsString();

				String latitud_Actual = nuevo.get("lat").getAsString();

				String longitud_actual = nuevo.get("lon").getAsString();

				LinkedList<String> arreglo_adyacentes = new LinkedList<String>();

				LinkedList<String> arreglo_infracciones = new LinkedList<String>();

				JsonArray adyacentes = nuevo.get("adj").getAsJsonArray();

				JsonArray infracciones = nuevo.get("infractions").getAsJsonArray();

				for (JsonElement adyacente_actual : adyacentes) {

					arreglo_adyacentes.add(adyacente_actual.getAsString());

				}

				for (JsonElement infraccion_actual : infracciones) {

					arreglo_infracciones.add(infraccion_actual.getAsString());

				}

				InterseccionMallaVial nuevo_vector = new InterseccionMallaVial(identificador_actual, latitud_Actual,
						longitud_actual, arreglo_infracciones, arreglo_adyacentes);	
//				Arcos_Calles nuevo_arco = new Arcos_Calles(identificador_actual, arreglo_adyacentes);
				vertices.enqueue(nuevo_vector);
//				arcos.enqueue(nuevo_arco);

			}

		}

	}

	public int loadJanuary(String archivoJanuary) {
		int c = 0;
		File archivo = new File(archivoJanuary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ",");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {

					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(0), datosActual.get(2),
						datosActual.get(3), datosActual.get(4), datosActual.get(5), datosActual.get(6),
						datosActual.get(7), datosActual.get(8), datosActual.get(9), datosActual.get(10),
						datosActual.get(11), datosActual.get(12), datosActual.get(13), datosActual.get(14),
						datosActual.get(15), datosActual.get(17), datosActual.get(18));

				tabla.put(datosActual.get(0), nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadFebruary(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ";");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(1), datosActual.get(3),
						datosActual.get(4), datosActual.get(5), datosActual.get(6), datosActual.get(7),
						datosActual.get(8), datosActual.get(9), datosActual.get(10), datosActual.get(11),
						datosActual.get(12), datosActual.get(13), datosActual.get(14), datosActual.get(15),
						datosActual.get(16), datosActual.get(18), datosActual.get(19));

				tabla.put(datosActual.get(0), nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadMarch(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ";");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(1), datosActual.get(3),
						datosActual.get(4), datosActual.get(5), datosActual.get(6), datosActual.get(7),
						datosActual.get(8), datosActual.get(9), datosActual.get(10), datosActual.get(11),
						datosActual.get(12), datosActual.get(13), datosActual.get(14), datosActual.get(15),
						datosActual.get(16), datosActual.get(18), datosActual.get(19));

				tabla.put(datosActual.get(0), nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadApril(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ";");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(1), datosActual.get(3),
						datosActual.get(4), datosActual.get(5), datosActual.get(6), datosActual.get(7),
						datosActual.get(8), datosActual.get(9), datosActual.get(10), datosActual.get(11),
						datosActual.get(12), datosActual.get(13), datosActual.get(14), datosActual.get(15),
						datosActual.get(16), datosActual.get(18), datosActual.get(19));

				tabla.put(datosActual.get(0), nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadMay(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ";");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(1), datosActual.get(3),
						datosActual.get(4), datosActual.get(5), datosActual.get(6), datosActual.get(7),
						datosActual.get(8), datosActual.get(9), datosActual.get(10), datosActual.get(11),
						datosActual.get(12), datosActual.get(13), datosActual.get(14), datosActual.get(15),
						datosActual.get(16), datosActual.get(18), datosActual.get(19));

				tabla.put(datosActual.get(0), nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadJune(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ";");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(1), datosActual.get(3),
						datosActual.get(4), datosActual.get(5), datosActual.get(6), datosActual.get(7),
						datosActual.get(8), datosActual.get(9), datosActual.get(10), datosActual.get(11),
						datosActual.get(12), datosActual.get(13), datosActual.get(14), datosActual.get(15),
						datosActual.get(16), datosActual.get(18), datosActual.get(19));

				tabla.put(datosActual.get(0), nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;

	}

	public int loadJuly(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ";");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(1), datosActual.get(3),
						datosActual.get(4), datosActual.get(5), datosActual.get(6), datosActual.get(7),
						datosActual.get(8), datosActual.get(9), datosActual.get(10), datosActual.get(11),
						datosActual.get(12), datosActual.get(13), datosActual.get(14), datosActual.get(15),
						datosActual.get(16), datosActual.get(18), datosActual.get(19));

				tabla.put(datosActual.get(0), nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;

	}

	public int loadAugust(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ";");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(1), datosActual.get(3),
						datosActual.get(4), datosActual.get(5), datosActual.get(6), datosActual.get(7),
						datosActual.get(8), datosActual.get(9), datosActual.get(10), datosActual.get(11),
						datosActual.get(12), datosActual.get(13), datosActual.get(14), datosActual.get(15),
						datosActual.get(16), datosActual.get(18), datosActual.get(19));

				tabla.put(datosActual.get(0), nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;

	}

	public int loadSeptember(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ";");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				try {

					VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(1), datosActual.get(3),
							datosActual.get(4), datosActual.get(5), datosActual.get(6), datosActual.get(7),
							datosActual.get(8), datosActual.get(9), datosActual.get(10), datosActual.get(11),
							datosActual.get(12), datosActual.get(13), datosActual.get(14), datosActual.get(15),
							datosActual.get(16), datosActual.get(18), datosActual.get(19));

					tabla.put(datosActual.get(0), nuevoViolations);
					c++;

				} catch (Exception e) {
					VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(0), datosActual.get(1),
							datosActual.get(3), datosActual.get(4), datosActual.get(5), datosActual.get(6),
							datosActual.get(7), datosActual.get(8), datosActual.get(9), datosActual.get(10),
							datosActual.get(11), datosActual.get(13), datosActual.get(14), datosActual.get(15),
							"SPEED 16-20 MPH OVER THE SPEED LIMIT", datosActual.get(17), datosActual.get(18));
					tabla.put(datosActual.get(0), nuevoViolations);
					c++;
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadOctober(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ";");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(1), datosActual.get(3),
						datosActual.get(4), datosActual.get(5), datosActual.get(6), datosActual.get(7),
						datosActual.get(8), datosActual.get(9), datosActual.get(10), datosActual.get(11),
						datosActual.get(12), datosActual.get(13), datosActual.get(14), datosActual.get(15),
						datosActual.get(16), datosActual.get(18), datosActual.get(19));

				tabla.put(datosActual.get(0), nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public int loadNovember(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ";");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(1), datosActual.get(3),
						datosActual.get(4), datosActual.get(5), datosActual.get(6), datosActual.get(7),
						datosActual.get(8), datosActual.get(9), datosActual.get(10), datosActual.get(11),
						datosActual.get(12), datosActual.get(13), datosActual.get(14), datosActual.get(15),
						datosActual.get(16), datosActual.get(18), datosActual.get(19));

				tabla.put(datosActual.get(0), nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;

	}

	public int loadDecember(String archivoFebruary) {
		int c = 0;
		File archivo = new File(archivoFebruary);
		try {
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String linea = sca.nextLine().replace(",,,", ",-1,-1,");
				linea = linea.replace(",,", ",-1,");
				linea = linea.replace(", ", "_");
				linea = linea.replace(";;", ";-1;");

				StringTokenizer st = new StringTokenizer(linea, ";");

				LinkedList<String> datosActual = new LinkedList<String>();

				while (st.hasMoreTokens()) {
					datosActual.add(st.nextToken());
				}

				VOMovingViolations nuevoViolations = new VOMovingViolations(datosActual.get(1), datosActual.get(3),
						datosActual.get(4), datosActual.get(5), datosActual.get(6), datosActual.get(7),
						datosActual.get(8), datosActual.get(9), datosActual.get(10), datosActual.get(11),
						datosActual.get(12), datosActual.get(13), datosActual.get(14), datosActual.get(15),
						datosActual.get(16), datosActual.get(18), datosActual.get(19));

				tabla.put(datosActual.get(0), nuevoViolations);
				c++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return c;
	}

	public LinearProbingHash<String,VOMovingViolations> darListaColaMovingViolations() {
		return tabla;
	}

	public LinearProbingHash<String,VOMovingViolations> darCopiaTabla() 
	{
		LinearProbingHash<String, VOMovingViolations> copia = new LinearProbingHash<String, VOMovingViolations>();

		Iterator<String> it = tabla.keys().iterator();
		
		while(it.hasNext()){
			String key_actual =  it.next();
			
			copia.put(tabla.get(key_actual).getAdressId()+"", tabla.get(key_actual));
			
			
		}
		
		return  copia;
		
		
	}

	public Queue<InterseccionMallaVial> darVertices() {
		return vertices;
	}

	public Queue<Arcos_Calles> darArcos() {
		return arcos;
	}

}