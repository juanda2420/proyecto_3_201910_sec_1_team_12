
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import javax.naming.ldap.SortResponseControl;

import org.json.simple.parser.ParseException;

<<<<<<< HEAD
import data_structures.BFS;
import data_structures.Dijkstra_algorithm;
=======
import data_structures.ArregloDinamico;
import data_structures.BFS;
>>>>>>> 95ccd9fe9b7e7230e398a6477ff8f2a2ff5a1fb1
import data_structures.Graph;
import data_structures.Greedy;
import data_structures.IQueue;
import data_structures.LinearProbingHash;
import data_structures.PriorityQueue;
import data_structures.Queue;
import data_structures.RedBlackBalancedSearchTrees;
<<<<<<< HEAD
=======

>>>>>>> 95ccd9fe9b7e7230e398a6477ff8f2a2ff5a1fb1
import logic.MovingViolationsManager;
import logic.VisualizacionMapa;
import view.MovingViolationsManagerView;
import vo.Arcos_Calles;
import vo.Coordenada;
import vo.InterseccionMallaVial;
import vo.VOMovingViolations;
import vo.arco_final;

public class Controller {

	// Componente vista (consola)

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 * 
	 */
	
	private LinearProbingHash<String, Integer> tablaIdNum ;
	private LinearProbingHash<Integer, String> tablaNumId ;

	private LinearProbingHash<String, VOMovingViolations> infracciones = new LinearProbingHash<String, VOMovingViolations>();

	private Queue<InterseccionMallaVial> vertices = new Queue<InterseccionMallaVial>();

	private Queue<Arcos_Calles> arcos = new Queue<Arcos_Calles>();
	
	private Queue<InterseccionMallaVial> verticesB1 = new Queue<InterseccionMallaVial>();

	private Queue<Arcos_Calles> arcosB1 = new Queue<Arcos_Calles>();

	private RedBlackBalancedSearchTrees<String, arco_final> arbol_arcos = new RedBlackBalancedSearchTrees<String, arco_final>();

	
	private Queue<InterseccionMallaVial> verticesB1 = new Queue<InterseccionMallaVial>();

	private Queue<Arcos_Calles> arcosB1 = new Queue<Arcos_Calles>();
	
	private LinearProbingHash<String, Integer> tablaIdNum ;
	private LinearProbingHash<Integer, String> tablaNumId ;
	
	int numeroDeArcosCola = 0;

	Graph grafo;

	VisualizacionMapa mapa;

	private MovingViolationsManagerView view;

	// Componente modelo (logica de la aplicacion)
	private MovingViolationsManager manager;

	public final static String VIOLATIONDESCR = "violationdescription";
	public final static String STREETID = "streetid";
	public final static String TICKETISSUEDATE = "ticketissuedate";
	public final static String XCOORD = "xcoord";
	public final static String YCOORD = "ycoord";
	public final static String FECHA = "fecha";
	public final static String RUTA_JSON_CENTRAL_WASHINTONDC = "./data/JSON_Central-WashingtonDC-OpenStreetMap.json";
	public final static String RUTA_JSON_MAPS = "./data/JSON_Map.json";

	public Controller() {
		view = new MovingViolationsManagerView();
		manager = new MovingViolationsManager();
		grafo = new Graph();

	}

	public void run() {
		long startTime;
		long endTime;
		long duration;

		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while (!fin) {
			view.printMenu();
			Controller controller = new Controller();
			int option = sc.nextInt();
			int idVertice1 = 0;
			int idVertice2 = 0;
			int[] datos = new int[12];

			switch (option) {

			case 0:
				String RutaArchivo = "";
				view.printMessage("Escoger el grafo a cargar: (1) Downtown  o (2)Ciudad Completa.");
				int ruta = sc.nextInt();
				if (ruta == 1) {
					RutaArchivo = RUTA_JSON_CENTRAL_WASHINTONDC;
				} else
					RutaArchivo = RUTA_JSON_MAPS;
				startTime = System.currentTimeMillis();

				try {
					// en esta parte intenta cargar el archivo JSON escogido

					datos = controller.loadMovingViolations();
					infracciones = controller.infracciones;

					if (RutaArchivo.equals(RUTA_JSON_CENTRAL_WASHINTONDC)) {
						manager.cargarJSON_Pequeno("./data/JSON_Central-WashingtonDC-OpenStreetMap.json");
						AsignarInfraccionesConGrafos();

					} else if (RutaArchivo.equals(RUTA_JSON_MAPS)) {
						manager.cargarJSON_Grande(RUTA_JSON_MAPS);

					}

				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				vertices = manager.darVertices();
				arcos = manager.darArcos();
				grafo = new Graph();
				cargarGrafo();
//				crearArcosFinales();
				System.out.println(
						" - - - - - -Informacion sobre el JSON - - - - - - - - - - -  - - - - - - - - - - - - - - -");

				if (RutaArchivo.equals(RUTA_JSON_CENTRAL_WASHINTONDC)) {
					System.out.println("se cargaron en total : " + arcos.size() + " Arcos con Highway (desde el xml)");
					numeroDeArcosCola = arcos.size();
				}

<<<<<<< HEAD
=======
				cargarGrafo();
				//cargarTablas();
>>>>>>> 95ccd9fe9b7e7230e398a6477ff8f2a2ff5a1fb1
				System.out.println("se cargaron en total : " + grafo.V() + " Vertices");
				System.out.println("se cargaron en total : " + grafo.E()
						+ " Arcos en el grafo (ya concectados todos los vertices con todos los arcos)");
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");

				view.printMessage(
						"- - - - - - - Informacion sobre las infracciones- - - - - - - - - - - - - - - - - - - - - - -");

				view.printMessage("Se cargaron " + datos[0] + " infracciones de enero");
				view.printMessage("Se cargaron " + datos[1] + " infracciones de febrero");
				view.printMessage("Se cargaron " + datos[2] + " infracciones de marzo");
				view.printMessage("Se cargaron " + datos[3] + " infracciones de abril");
				view.printMessage("Se cargaron " + datos[4] + " infracciones de mayo");
				view.printMessage("Se cargaron " + datos[5] + " infracciones de junio");
				view.printMessage("Se cargaron " + datos[6] + " infracciones de julio");
				view.printMessage("Se cargaron " + datos[7] + " infracciones de agosto");
				view.printMessage("Se cargaron " + datos[8] + " infracciones de septiembre");
				view.printMessage("Se cargaron " + datos[9] + " infracciones de octubre");
				view.printMessage("Se cargaron " + datos[10] + " infracciones de noviembre");
				view.printMessage("Se cargaron " + datos[11] + " infracciones de diciembre");
				int total = 0;
				for (int i = 0; i < datos.length; i++) {
					total = total + datos[i];
				}
				view.printMessage("Se cargaron un total de: " + total + " infracciones en el aÃ±o.");

				break;

			case 1:
				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				idVertice1 = sc.nextInt();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				idVertice2 = sc.nextInt();

				startTime = System.currentTimeMillis();
				caminoCostoMinimoA1(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar el camino a seguir con sus vÃ©rtices
				 * (Id, UbicaciÃ³n GeogrÃ¡fica), el costo mÃ­nimo (menor
				 * cantidad de infracciones), y la distancia estimada (en Km).
				 * 
				 * TODO Google Maps: Mostrar el camino resultante en Google Maps
				 * (incluyendo la ubicaciÃ³n de inicio y la ubicaciÃ³n de
				 * destino).
				 */
				break;

			case 2:
				view.printMessage(
						"2A. Consultar los N vï¿½rtices con mayor nï¿½mero de infracciones. Ingrese el valor de N: ");
				int n = sc.nextInt();

				startTime = System.currentTimeMillis();
				mayorNumeroVerticesA2(n);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar la informacion de los n vertices (su
				 * identificador, su ubicaciÃ³n (latitud, longitud), y el total
				 * de infracciones) Mostra el nÃºmero de componentes conectadas
				 * (subgrafos) y los identificadores de sus vertices
				 * 
				 * TODO Google Maps: Marcar la localizaciÃ³n de los vÃ©rtices
				 * resultantes en un mapa en Google Maps usando un color 1.
				 * Destacar la componente conectada mÃ¡s grande (con mÃ¡s
				 * vÃ©rtices) usando un color 2.
				 */
				break;

			case 3:

				view.printMessage("Ingrese El id del primer vertice (Ej. 1004839834): ");
				idVertice1 = sc.nextInt();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 1004839836): ");
				idVertice2 = sc.nextInt();
				Graph req3;
				VisualizacionMapa map3;
				
				startTime = System.currentTimeMillis();
				req3 = caminoLongitudMinimoaB1(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");

				/*
				 * TODO Consola: Mostrar el camino a seguir, informando el total de vértices,
				 * sus vértices (Id, Ubicación Geográfica) y la distancia estimada (en Km).
				 * 
				 * TODO Google Maps: Mostre el camino resultante en Google Maps (incluyendo la
				 * ubicación de inicio y la ubicación de destino).
				 */
				view.printMessage("Numero total de vertices: " +req3.V());
				RedBlackBalancedSearchTrees<String, InterseccionMallaVial> vr3 = req3.darVertices(); 
				Iterator<String> itera = vr3.keys().iterator();
				while(itera.hasNext())
				{
					String idActual = itera.next();
					view.printMessage("ID: "+idActual +" | Latitud: "+ vr3.get(idActual).getLatitud() + " | Longitud: " + vr3.get(idActual).getLongitud());
				}
				view.printMessage("Distancia estimada: " + " Km");
				
				map3 = new VisualizacionMapa(req3);
				map3.visualizarMapa();
				
				break;


			case 4:
				double lonMin;
				double lonMax;
				view.printMessage("Ingrese la longitud minima (Ej. -77,0448967): ");
				lonMin = sc.nextDouble();
				view.printMessage("Ingrese la longitud m�xima (Ej. -76,9954787): ");
				lonMax = sc.nextDouble();

				view.printMessage("Ingrese la latitud minima (Ej. 38,8909806): ");
				double latMin = sc.nextDouble();
				view.printMessage("Ingrese la latitud m�xima (Ej. 38,9147987): ");
				double latMax = sc.nextDouble();

				view.printMessage("Ingrese el n�mero de columnas");
				int columnas = sc.nextInt();
				view.printMessage("Ingrese el n�mero de filas");
				int filas = sc.nextInt();
				
				Graph req4;
				VisualizacionMapa map4;

				startTime = System.currentTimeMillis();
				req4 = definirCuadriculaB2(lonMin, lonMax, latMin, latMax, columnas, filas);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar el número de vértices en el grafo resultado de la
				 * aproximación. Mostar el identificador y la ubicación geográfica de cada uno
				 * de estos vértices.
				 * 
				 * TODO Google Maps: Marcar las ubicaciones de los vértices resultantes de la
				 * aproximación de la cuadrícula en Google Maps.
				 */

				view.printMessage("Numero total de vertices: " +req4.V());
				RedBlackBalancedSearchTrees<String, InterseccionMallaVial> vr4 = req4.darVertices(); 
				Iterator<String> itera4 = vr4.keys().iterator();
				while(itera4.hasNext())
				{
					String idActual = itera4.next();
					view.printMessage("ID: "+idActual +" | Latitud: "+ vr4.get(idActual).getLatitud() + " | Longitud: " + vr4.get(idActual).getLongitud());
				}
				
				map4 = new VisualizacionMapa(req4);
				map4.visualizarMapa();
				
				break;

			case 5:

				startTime = System.currentTimeMillis();
				arbolMSTKruskalC1();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar los vÃ©rtices (identificadores), los
				 * arcos incluidos (Id vÃ©rtice inicial e Id vÃ©rtice final), y
				 * el costo total (distancia en Km) del Ã¡rbol.
				 * 
				 * TODO Google Maps: Mostrar el Ã¡rbol generado resultante en
				 * Google Maps: sus vÃ©rtices y sus arcos.
				 */

				break;

			case 6:

				startTime = System.currentTimeMillis();
				arbolMSTPrimC2();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar los vÃ©rtices (identificadores), los
				 * arcos incluidos (Id vÃ©rtice inicial e Id vÃ©rtice final), y
				 * el costo total (distancia en Km) del Ã¡rbol.
				 * 
				 * TODO Google Maps: Mostrar el Ã¡rbol generado resultante en
				 * Google Maps: sus vÃ©rtices y sus arcos.
				 */
				break;

			case 7:

				startTime = System.currentTimeMillis();
				caminoCostoMinimoDijkstraC3();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar de cada camino resultante: su secuencia
				 * de vÃ©rtices (identificadores) y su costo (distancia en Km).
				 * 
				 * TODO Google Maps: Mostrar los caminos de costo mÃ­nimo en
				 * Google Maps: sus vÃ©rtices y sus arcos. Destaque el camino
				 * mÃ¡s largo (en distancia) usando un color diferente
				 */
				break;

			case 8:
				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				idVertice1 = sc.nextInt();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				idVertice2 = sc.nextInt();

				startTime = System.currentTimeMillis();
				caminoMasCortoC4(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				 * TODO Consola: Mostrar del camino resultante: su secuencia de
				 * vÃ©rtices (identificadores), el total de infracciones y la
				 * distancia calculada (en Km).
				 * 
				 * TODO Google Maps: Mostrar el camino resultante en Google
				 * Maps: sus vÃ©rtices y sus arcos.
				 */
				break;

			case 9:
				fin = true;
				sc.close();
				break;
			}
		}
	}

	public void cargarGrafo() {

		while (!vertices.isEmpty()) {

			InterseccionMallaVial actual = vertices.dequeue();

			grafo.addVertex(actual.objectId(), actual);
			verticesB1.enqueue(actual);
		}
<<<<<<< HEAD
				
		Iterator<String> it = grafo.darVertices().keys().iterator();
		
		while(it.hasNext()){
			String id_vector_actual = it.next();
			
			InterseccionMallaVial vector_actual =  grafo.darVertices().get(id_vector_actual);
			
			LinkedList<String> identificafores_adyacentes  = vector_actual.darAdyacentes();
			
			for (int i = 0; i < identificafores_adyacentes.size(); i++) {
				
				String id_adyacente_siguiente = identificafores_adyacentes.get(i);
				
				InterseccionMallaVial vector_siguiente = grafo.darVertices().get(id_adyacente_siguiente);
				
				
				arco_final nuevo_arco = new arco_final(vector_actual,vector_siguiente);
				
				vector_actual.añadir_arcos_adyacentes(nuevo_arco);
				arbol_arcos.put(nuevo_arco.getIdentificador(), nuevo_arco);
				
=======

		while (!arcos.isEmpty()) {

			Arcos_Calles arcoActual = arcos.dequeue();
			arcosB1.enqueue(arcoActual);
			for (int i = 0; i < arcoActual.darVertices().size(); i++) {

				String verticeActual = arcoActual.darVertices().get(i);

				for (int j = i + 1; j < arcoActual.darVertices().size(); j++) {

					String verticeSegundo = arcoActual.darVertices().get(j);

					grafo.addEdge2(verticeActual, verticeSegundo, arcoActual);
				}

>>>>>>> 95ccd9fe9b7e7230e398a6477ff8f2a2ff5a1fb1
			}
		}
		
<<<<<<< HEAD
		grafo.asignarNuevosArcos(arbol_arcos);
=======
		
	}
	
	public void cargarTablas()
	{
		tablaIdNum = new LinearProbingHash<String, Integer>(grafo.V());
		tablaNumId = new LinearProbingHash<Integer, String>(grafo.V());
		
		RedBlackBalancedSearchTrees<String, InterseccionMallaVial> vertices = grafo.darVertices();
		Queue<String> ids = new Queue<String>();
		Iterator<String> iterator = vertices.keys().iterator();
		while(iterator.hasNext())
		{
			ids.enqueue(iterator.next());
		}
		for(int i = 0; i < grafo.V(); i++)
		{
			String id = ids.dequeue();
			tablaIdNum.put(id, i);
			tablaNumId.put(i, id);
		}
		
>>>>>>> 95ccd9fe9b7e7230e398a6477ff8f2a2ff5a1fb1
	}

//	public void crearArcosFinales() {
//		
//		Iterator<String> it = grafo.darArcos().keys().iterator();
//		
//		while(it.hasNext()){
//			
//			String llaveActual = it.next();
//			
//			Arcos_Calles arcoActual = grafo.darArcos().get(llaveActual);
//			
//			for (int i = 0; i < arcoActual.darVertices().size(); i++) {
//
//				String verticeActual = arcoActual.darVertices().get(i);
//
//				for (int j = i + 1; j < arcoActual.darVertices().size(); j++) {
//
//					String verticeSegundo = arcoActual.darVertices().get(j);
//
//					arco_final nuevo_arco = new arco_final(grafo.darVertices().get(verticeActual),grafo.darVertices().get(verticeSegundo));				
//				
//					if (arbol_arcos.contains(nuevo_arco.getIdentificador()) == false) {					
//
//						arbol_arcos.put(nuevo_arco.getIdentificador(), nuevo_arco);
//						
//					}
//
//				}
//				
//
//			}
//			
//			
//		}
//
//	}
	
	
//	public void meter_adyacentes_por_cada_vertice(){
//		
//		Iterator<String> it = arbol_arcos.keys().iterator();
//		
//		while(it.hasNext()){
//			
//			String id_actual = it.next();
//			
//			arco_final actual  =arbol_arcos.get(id_actual);
//			
//			String interseccion_actual_identificador = actual.darVertice1().objectId();
//			
//			InterseccionMallaVial vector_actual = grafo.darVertices().get(interseccion_actual_identificador);
//			
//			vector_actual.añadir_arcos_adyacentes(actual);
//			
//			grafo.darVertices().delete(interseccion_actual_identificador);
//			
//			grafo.darVertices().put(interseccion_actual_identificador, vector_actual);
//			
//		}
//		
//		
//	}

	public int[] loadMovingViolations() {

		int[] resp = new int[12];

		resp[0] = manager.loadJanuary("./data/January_wgs84.csv");
		resp[1] = manager.loadFebruary("./data/February_wgs84.csv");
		resp[2] = manager.loadMarch("./data/March_wgs84.csv");
		resp[3] = manager.loadApril("./data/Abril_wgs84.csv");
		resp[4] = manager.loadMay("./data/May_wgs84.csv");
		resp[5] = manager.loadJune("./data/June_wgs84.csv");
		resp[6] = manager.loadJuly("./data/July_wgs84.csv");
		resp[7] = manager.loadAugust("./data/August_wgs84.csv");
		resp[8] = manager.loadSeptember("./data/September_wgs84.csv");
		resp[9] = manager.loadOctober("./data/October_wgs84.csv");
		resp[10] = manager.loadNovember("./data/November_wgs84.csv");
		resp[11] = manager.loadDecember("./data/December_wgs84.csv");
		infracciones = manager.darListaColaMovingViolations();

		return resp;
	}

	public double CalcularDistanciaHarversiana(double lat1, double lng1, double lat2, double lng2) {
		// double radioTierra = 3958.75;//en millas
		double radioTierra = 6371;// en kilÃ³metros
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2);
		double va1 = Math.pow(sindLat, 2)
				+ Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
		double distancia = radioTierra * va2;

		return distancia;
	}

	// PARA EL GRAFO DEL JSON PEQUEÃ‘O
	public void AsignarInfraccionesConGrafos() {

		Iterator<String> it = grafo.darVertices().keys().iterator();

		while (it.hasNext()) {

			String llaveActual = it.next();

			InterseccionMallaVial VerticeActual = grafo.darVertices().get(llaveActual);

			VOMovingViolations RTA = null;

			double referenciaMasCercano = 0;

			Queue<VOMovingViolations> pruebaNueva = new Queue<VOMovingViolations>();

			Iterator<String> it2 = infracciones.keys().iterator();

			int contador = 0;

			while (it2.hasNext()) {

				String llave_actual = it2.next();

				VOMovingViolations InfraccionActual = infracciones.get(llave_actual);

				double calculoActual = CalcularDistanciaHarversiana(VerticeActual.getLatitud(),
						VerticeActual.getLongitud(),
						Double.parseDouble(InfraccionActual.getLatitud().replace(",", ".")),
						Double.parseDouble(InfraccionActual.getLongitud().replace(",", ".")));

				if (contador == 0) {
					referenciaMasCercano = calculoActual;
					RTA = InfraccionActual;
				}

				else {
					if (referenciaMasCercano > calculoActual) {
						referenciaMasCercano = calculoActual;
						RTA = InfraccionActual;
					}
				}

				pruebaNueva.enqueue(InfraccionActual);
				contador++;
			}

			VerticeActual.setInfraccion(RTA);

			grafo.setInfoVertex(VerticeActual.objectId(), VerticeActual);

			for (int i = 0; i < pruebaNueva.size(); i++) {

				VOMovingViolations Actual = pruebaNueva.dequeue();

				infracciones.put(Actual.getAdressId() + "", Actual);
			}

		}

	}
	// PARA EL GRAFO DEL JSON GRANDE
	// public void AsignarInfraccionesConGrafos2() {
	//
	// Iterator<String> it = grafo.darVertices().keys().iterator();
	//
	// int contador =0;
	//
	// while (it.hasNext()) {
	//
	// String llaveActual = it.next();
	//
	// InterseccionMallaVial VerticeActual =
	// grafo.darVertices().get(llaveActual);
	//
	// LinkedList<String> lista_infracciones_actual =
	// VerticeActual.getInfracciones();
	//
	// VOMovingViolations RTA = null;
	//
	// double referenciaMasCercano = 0;
	//
	// for (int i = 0; i < lista_infracciones_actual.size(); i++) {
	//
	// String identificador_infraccion_actual =
	// lista_infracciones_actual.get(i);
	//
	// VOMovingViolations infraccion =
	// infracciones.get(identificador_infraccion_actual);
	//
	// double calculoActual =
	// CalcularDistanciaHarversiana(VerticeActual.getLatitud(),
	// VerticeActual.getLongitud(),
	// Double.parseDouble(infraccion.getLatitud().replace(",", ".")),
	// Double.parseDouble(infraccion.getLongitud().replace(",", ".")));
	//
	// if (contador == 0) {
	// referenciaMasCercano = calculoActual;
	// RTA = infraccion;
	// }
	//
	// else {
	// if (referenciaMasCercano > calculoActual) {
	// referenciaMasCercano = calculoActual;
	// RTA = infraccion;
	// }
	// }
	// }
	//
	// VerticeActual.setInfraccion(RTA);
	//
	// grafo.setInfoVertex(VerticeActual.objectId(), VerticeActual);
	//
	//
	// }
	// }

	// TODO El tipo de retorno de los mï¿½todos puede ajustarse segï¿½n la
	// conveniencia
	/**
	 * Requerimiento 1A: Encontrar el camino de costo mï¿½nimo para un viaje
	 * entre dos ubicaciones geogrï¿½ficas.
	 * 
	 * @param idVertice2
	 * @param idVertice1
	 * 
	 */
	
	 public void caminoCostoMinimoA1(int idVertice1, int idVertice2) {
			
			
	 InterseccionMallaVial primero = grafo.getInfoVertex(idVertice1+"");
	 InterseccionMallaVial segundo = grafo.getInfoVertex(idVertice2+"");
	
	
	 Dijkstra_algorithm dijkstra_nuevo = new
	 Dijkstra_algorithm(grafo.darVertices(), grafo.darArcos());
	
	 dijkstra_nuevo.execute(primero);
	
	 LinkedList<InterseccionMallaVial> camino =
	 dijkstra_nuevo.getPath(segundo);
	
		int sumaDeCosto = 0;
		Double sumaDeDistancia = 0.0;
		boolean terminar = false;

		System.out.println("El camino a seguir es : ");
		System.out.println(" info vertices : ");

		for (int i = 0; i < camino.size() && !terminar; i++) {

			if (i + 1 == camino.size()) {
				terminar = true;
			}

			else {

				InterseccionMallaVial actual = camino.get(i);

				InterseccionMallaVial siguiente = camino.get(i + 1);

				sumaDeCosto = sumaDeCosto + actual.getCantidadInfracciones();

				sumaDeDistancia = sumaDeDistancia + CalcularDistanciaHarversiana(actual.getLatitud(),
						actual.getLatitud(), siguiente.getLatitud(), siguiente.getLongitud());

				System.out.println(actual.toString());

			}

		}

		System.out.println("El costo total es de : " + sumaDeCosto);
		System.out.println(
				"La distancia (en kilometros) desde el vertice inicial al vertice final dados, recorriendo el camino dado es de : "
						+ sumaDeDistancia);
	
	
	
	 }
//	public void caminoCostoMinimoA1(int idVertice1, int idVertice2) {
//
//
//		Greedy greddy_actual = new Greedy();
//
//
//		ArrayList<InterseccionMallaVial> camino = greddy_actual.Greedy(idVertice1+"", idVertice2+"", this.grafo);
//
//		int sumaDeCosto = 0;
//		Double sumaDeDistancia = 0.0;
//		boolean terminar = false;
//
//		System.out.println("El camino a seguir es : ");
//		System.out.println(" info vertices : ");
//
//		for (int i = 0; i < camino.size() && !terminar; i++) {
//
//			if (i + 1 == camino.size()) {
//				terminar = true;
//			}
//
//			else {
//
//				InterseccionMallaVial actual = camino.get(i);
//
//				InterseccionMallaVial siguiente = camino.get(i + 1);
//
//				sumaDeCosto = sumaDeCosto + actual.getCantidadInfracciones();
//
//				sumaDeDistancia = sumaDeDistancia + CalcularDistanciaHarversiana(actual.getLatitud(),
//						actual.getLatitud(), siguiente.getLatitud(), siguiente.getLongitud());
//
//				System.out.println(actual.toString());
//
//			}
//
//		}
//
//		System.out.println("El costo total es de : " + sumaDeCosto);
//		System.out.println(
//				"La distancia (en kilometros) desde el vertice inicial al vertice final dados, recorriendo el camino dado es de : "
//						+ sumaDeDistancia);
//
//	}
<<<<<<< HEAD
=======
	
	
	 public static void calcular_caminos_dijkstra(InterseccionMallaVial source)
	    {
//	        source.setDistanciaMinima(0.);
//	        PriorityQueue<InterseccionMallaVial> vertexQueue = new PriorityQueue<InterseccionMallaVial>();
//	        vertexQueue.insert(source);
//
//	        while (!vertexQueue.isEmpty()) {
//	            InterseccionMallaVial u = vertexQueue.delMax();
//
//	            // Visit each edge exiting u
//	            for (InterseccionMallaVial e : u.getInfracciones())
//	            {
//	            	InterseccionMallaVial v = e.target;
//	                double weight = e.weight;
//	                double distanceThroughU = u.minDistance + weight;
//	                if (distanceThroughU < v.minDistance) {
//	                    vertexQueue.remove(v);
//
//	                    v.minDistance = distanceThroughU ;
//	                    v.previous = u;
//	                    vertexQueue.add(v);
//	                }
//	            }
//	        }
	    }

	    public static List<InterseccionMallaVial> getShortestPathTo(InterseccionMallaVial target)
	    {
	        List<InterseccionMallaVial> path = new ArrayList<InterseccionMallaVial>();
//	        for (InterseccionMallaVial vertex = target; vertex != null; vertex = vertex.previous)
//	            path.add(vertex);
//
//	        Collections.reverse(path);
	        return path;
	    }
>>>>>>> 95ccd9fe9b7e7230e398a6477ff8f2a2ff5a1fb1



	// TODO El tipo de retorno de los mï¿½todos puede ajustarse segï¿½n la
	// conveniencia
	/**
	 * Requerimiento 2A: Determinar los n vï¿½rtices con mayor nï¿½mero de
	 * infracciones. Adicionalmente identificar las componentes conectadas
	 * (subgrafos) que se definan ï¿½nicamente entre estos n vï¿½rtices
	 * 
	 * @param int
	 *            n: numero de vertices con mayor numero de infracciones
	 */
	public void mayorNumeroVerticesA2(int n) {
		
		Iterator<String> it2  = grafo.darVertices().keys().iterator();

		while(it2.hasNext()){
			
			String id = it2.next();
			
			InterseccionMallaVial actual  = grafo.darVertices().get(id);
			
			System.out.println(actual.getCantidadInfracciones());
			
		}
		
		
		int contador =0;
		
		RedBlackBalancedSearchTrees<String, InterseccionMallaVial> copia = grafo.darVertices();
		
		LinkedList<InterseccionMallaVial> RTA = new LinkedList<InterseccionMallaVial>();
		
		LinkedList<InterseccionMallaVial> sortear = new LinkedList<InterseccionMallaVial>();
		
		Iterator<String> it = copia.keys().iterator();
		
		Comparator<InterseccionMallaVial> comp = new Comparator<InterseccionMallaVial>() {

			@Override
			public int compare(InterseccionMallaVial o1, InterseccionMallaVial o2) {
				if (o1.getCantidadInfracciones() > o2.getCantidadInfracciones()){
					return 1;
				}
				else if ((o1.getCantidadInfracciones() < o2.getCantidadInfracciones())){
					return -1;
				}
				else return 0;
			}
			
		};
		
		while(it.hasNext()){
			
			sortear.add(copia.get(it.next()));
			
		}
			
			sortear.sort(comp);
			
	
			while(contador < n)
			{
				RTA.add(sortear.getLast());
				contador++;
			}
			

			

	}
	

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la
<<<<<<< HEAD
		// conveniencia
		/**
		 * Requerimiento 1B: Encontrar el camino m�s corto para un viaje entre dos
		 * ubicaciones geogr�ficas
		 * 
		 * @param idVertice2
		 * @param idVertice1
		 */
		public Graph caminoLongitudMinimoaB1(int idVertice1, int idVertice2) 
		{
			Graph grafoReq3 = new Graph();
			Graph grafoTemp = new Graph();
			int in = 0;
			while ( !verticesB1.isEmpty() ) {
				
				InterseccionMallaVial actual = verticesB1.dequeue();

				grafoTemp.addVertex(in+"", actual);
				in++;
				
			}

			while (!arcosB1.isEmpty()) {
=======
	// conveniencia
	/**
	 * Requerimiento 1B: Encontrar el camino m�s corto para un viaje entre dos
	 * ubicaciones geogr�ficas
	 * 
	 * @param idVertice2
	 * @param idVertice1
	 */
	public Graph caminoLongitudMinimoaB1(int idVertice1, int idVertice2) 
	{
		Graph grafoReq3 = new Graph();
		Graph grafoTemp = new Graph();
		int in = 0;
		while ( !verticesB1.isEmpty() ) {
			
			InterseccionMallaVial actual = verticesB1.dequeue();

			grafoTemp.addVertex(in+"", actual);
			in++;
			
		}

		while (!arcosB1.isEmpty()) {

			Arcos_Calles arcoActual = arcosB1.dequeue();

			for (int i = 0; i < arcoActual.darVertices().size(); i++) {

				String verticeActual = arcoActual.darVertices().get(i);
				int vA = tablaIdNum.get(verticeActual);
				
				for (int j = i + 1; j < arcoActual.darVertices().size(); j++) {

					String verticeSegundo = arcoActual.darVertices().get(j);
					int vS = tablaIdNum.get(verticeSegundo);
					grafoTemp.addEdge2(vA+"", vS+"", arcoActual);
				}

			}

		}
//		RedBlackBalancedSearchTrees<String, InterseccionMallaVial> t = grafoTemp.darVertices();
//		Iterator<String> iterator1 = t.keys().iterator();
//		int k = 0;
//		while(iterator1.hasNext() && k <100)
//		{
//			System.out.println(iterator1.next());
//			k++;
//		}
		
		
		int indiceID1 = tablaIdNum.get(idVertice1+"");
		int indiceID2 = tablaIdNum.get(idVertice2+"");
		
		BFS bfs = new BFS(grafoTemp, indiceID1);
		Iterable<Integer> shortp = bfs.pathTo(indiceID2);
		Iterator<Integer> iterator = shortp.iterator();
		while(iterator.hasNext())
		{
			System.out.println(iterator.next());
		}
		return grafoReq3;
	}

	// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la
	// conveniencia
	/**
	 * Requerimiento 2B: Definir una cuadricula regular de N columnas por M filas.
	 * que incluya las longitudes y latitudes dadas
	 * 
	 * @param lonMin: Longitud minima presente dentro de la cuadricula
	 * @param lonMax: Longitud maxima presente dentro de la cuadricula
	 * @param latMin: Latitud minima presente dentro de la cuadricula
	 * @param latMax: Latitud maxima presente dentro de la cuadricula
	 * @param columnas: Numero de columnas de la cuadricula
	 * @param filas: Numero de filas de la cuadricula
	 */
	public Graph definirCuadriculaB2(double lonMin, double lonMax, double latMin, double latMax, int columnas, int filas) 
	{
		// TODO Auto-generated method stub
		
		Graph grafoReq4 = new Graph();
		
		Coordenada coorMin = new Coordenada(lonMin,latMin);
		Coordenada coorMax = new Coordenada(lonMax, latMax);
		
		Double difLongitudes=(lonMax-lonMin)/(columnas-1);
		difLongitudes = Math.round(difLongitudes * 10000) / 10000d;

		Double difLatitudes=(latMax-latMin)/(filas-1);
		difLatitudes = Math.round(difLatitudes * 10000) / 10000d;

		int numIntersecciones=filas*columnas;

		Double[] arregloLat=new Double[numIntersecciones];
		Double[] arregloLon=new Double[numIntersecciones];
		
		//Ciclo para guardar todas las coordenadas en los dos arreglos
		Double tmpLatitudes=0.0;
		Double tmpLongitudes=0.0;
		Double sumaLon=0.0;
		Double sumaLat=0.0;
		int contIndice=0;
		for (int i = 0; i < filas ; i++) 
		{
			tmpLatitudes+=difLatitudes*(i);
			tmpLongitudes=0.0;
			for (int j = 0; j < columnas ; j++) 
			{	
				tmpLongitudes += difLongitudes*(j);
				sumaLat=latMin+tmpLatitudes;
				sumaLat = Math.round(sumaLat * 10000) / 10000d;
				sumaLon=lonMin+tmpLongitudes;
				sumaLon = Math.round(sumaLon * 10000) / 10000d;
				arregloLon[contIndice]=sumaLon;
				arregloLat[contIndice]=sumaLat;
				contIndice++;
			}
		}
		
		Coordenada[] matriz = new Coordenada[numIntersecciones];
		
			
		//Ciclo obtener un Arreglo de con las coordenadas de la matriz sobre un area de interes
		for (int i = 0; i < numIntersecciones; i++) 
		{
			Coordenada x = new Coordenada(arregloLat[i], arregloLon[i]);
			matriz[i] = x;
		}
		
		//Compara con los vertices del Grafo y da un arreglo de los vertices mas cercanos de el


		
		String[] verticesAprox= new String[numIntersecciones];

		for (int i = 0; i < numIntersecciones; i++) 
		{
			RedBlackBalancedSearchTrees<String, InterseccionMallaVial> vertices = grafo.darVertices();
			Iterator<String> llaves = vertices.keys().iterator();
			Coordenada x = matriz[i];
			String aprox = null;
			Double menorDist = Double.MAX_VALUE;
			while (llaves.hasNext()) 
			{
				String act = llaves.next(); 
				    Coordenada y = new Coordenada(grafo.getInfoVertex(act).getLongitud(),grafo.getInfoVertex(act).getLatitud());
					Double dist = CalcularDistanciaHarversiana(x.darLatitud(), x.darLongitud(), y.darLatitud(), y.darLongitud());
					
					if(dist < menorDist)
					{
						aprox = act;
						menorDist=dist;
					}
			}

			verticesAprox[i] = aprox;
		}
		System.out.println(verticesAprox.length);
		System.out.println(verticesAprox[0]);
		System.out.println(verticesAprox[1]);
		System.out.println(verticesAprox[2]);
		System.out.println(verticesAprox[3]);
		
		for(int i = 0; i < verticesAprox.length; i++)
		{
			InterseccionMallaVial verticeNuevo = grafo.getInfoVertex(verticesAprox[i]);
			grafoReq4.addVertex(verticeNuevo.objectId(), verticeNuevo);
		}
		
		return grafoReq4;
	}
	
	
	
	
	
	
>>>>>>> 95ccd9fe9b7e7230e398a6477ff8f2a2ff5a1fb1

				Arcos_Calles arcoActual = arcosB1.dequeue();

				for (int i = 0; i < arcoActual.darVertices().size(); i++) {

					String verticeActual = arcoActual.darVertices().get(i);
					int vA = tablaIdNum.get(verticeActual);
					
					for (int j = i + 1; j < arcoActual.darVertices().size(); j++) {

						String verticeSegundo = arcoActual.darVertices().get(j);
						int vS = tablaIdNum.get(verticeSegundo);
//						grafoTemp.addEdge(vA+"", vS+"", arcoActual);
					}

				}

			}
//			RedBlackBalancedSearchTrees<String, InterseccionMallaVial> t = grafoTemp.darVertices();
//			Iterator<String> iterator1 = t.keys().iterator();
//			int k = 0;
//			while(iterator1.hasNext() && k <100)
//			{
//				System.out.println(iterator1.next());
//				k++;
//			}
			
			
			int indiceID1 = tablaIdNum.get(idVertice1+"");
			int indiceID2 = tablaIdNum.get(idVertice2+"");
			
			BFS bfs = new BFS(grafoTemp, indiceID1);
			Iterable<Integer> shortp = bfs.pathTo(indiceID2);
			Iterator<Integer> iterator = shortp.iterator();
			while(iterator.hasNext())
			{
				System.out.println(iterator.next());
			}
			return grafoReq3;
		}
		// TODO El tipo de retorno de los m�todos puede ajustarse seg�n la
		// conveniencia
		/**
		 * Requerimiento 2B: Definir una cuadricula regular de N columnas por M filas.
		 * que incluya las longitudes y latitudes dadas
		 * 
		 * @param lonMin: Longitud minima presente dentro de la cuadricula
		 * @param lonMax: Longitud maxima presente dentro de la cuadricula
		 * @param latMin: Latitud minima presente dentro de la cuadricula
		 * @param latMax: Latitud maxima presente dentro de la cuadricula
		 * @param columnas: Numero de columnas de la cuadricula
		 * @param filas: Numero de filas de la cuadricula
		 */
		public Graph definirCuadriculaB2(double lonMin, double lonMax, double latMin, double latMax, int columnas, int filas) 
		{
			// TODO Auto-generated method stub
			
			Graph grafoReq4 = new Graph();
			
			Coordenada coorMin = new Coordenada(lonMin,latMin);
			Coordenada coorMax = new Coordenada(lonMax, latMax);
			
			Double difLongitudes=(lonMax-lonMin)/(columnas-1);
			difLongitudes = Math.round(difLongitudes * 10000) / 10000d;

			Double difLatitudes=(latMax-latMin)/(filas-1);
			difLatitudes = Math.round(difLatitudes * 10000) / 10000d;

			int numIntersecciones=filas*columnas;

			Double[] arregloLat=new Double[numIntersecciones];
			Double[] arregloLon=new Double[numIntersecciones];
			
			//Ciclo para guardar todas las coordenadas en los dos arreglos
			Double tmpLatitudes=0.0;
			Double tmpLongitudes=0.0;
			Double sumaLon=0.0;
			Double sumaLat=0.0;
			int contIndice=0;
			for (int i = 0; i < filas ; i++) 
			{
				tmpLatitudes+=difLatitudes*(i);
				tmpLongitudes=0.0;
				for (int j = 0; j < columnas ; j++) 
				{	
					tmpLongitudes += difLongitudes*(j);
					sumaLat=latMin+tmpLatitudes;
					sumaLat = Math.round(sumaLat * 10000) / 10000d;
					sumaLon=lonMin+tmpLongitudes;
					sumaLon = Math.round(sumaLon * 10000) / 10000d;
					arregloLon[contIndice]=sumaLon;
					arregloLat[contIndice]=sumaLat;
					contIndice++;
				}
			}
			
			Coordenada[] matriz = new Coordenada[numIntersecciones];
			
				
			//Ciclo obtener un Arreglo de con las coordenadas de la matriz sobre un area de interes
			for (int i = 0; i < numIntersecciones; i++) 
			{
				Coordenada x = new Coordenada(arregloLat[i], arregloLon[i]);
				matriz[i] = x;
			}
			
			//Compara con los vertices del Grafo y da un arreglo de los vertices mas cercanos de el


			
			String[] verticesAprox= new String[numIntersecciones];

			for (int i = 0; i < numIntersecciones; i++) 
			{
				RedBlackBalancedSearchTrees<String, InterseccionMallaVial> vertices = grafo.darVertices();
				Iterator<String> llaves = vertices.keys().iterator();
				Coordenada x = matriz[i];
				String aprox = null;
				Double menorDist = Double.MAX_VALUE;
				while (llaves.hasNext()) 
				{
					String act = llaves.next(); 
					    Coordenada y = new Coordenada(grafo.getInfoVertex(act).getLongitud(),grafo.getInfoVertex(act).getLatitud());
						Double dist = CalcularDistanciaHarversiana(x.darLatitud(), x.darLongitud(), y.darLatitud(), y.darLongitud());
						
						if(dist < menorDist)
						{
							aprox = act;
							menorDist=dist;
						}
				}

				verticesAprox[i] = aprox;
			}
			System.out.println(verticesAprox.length);
			System.out.println(verticesAprox[0]);
			System.out.println(verticesAprox[1]);
			System.out.println(verticesAprox[2]);
			System.out.println(verticesAprox[3]);
			
			for(int i = 0; i < verticesAprox.length; i++)
			{
				InterseccionMallaVial verticeNuevo = grafo.getInfoVertex(verticesAprox[i]);
				grafoReq4.addVertex(verticeNuevo.objectId(), verticeNuevo);
			}
			
			return grafoReq4;
		}
		
		

	// TODO El tipo de retorno de los mï¿½todos puede ajustarse segï¿½n la
	// conveniencia
	/**
	 * Requerimiento 1C: Calcular un ï¿½rbol de expansiï¿½n mï¿½nima (MST) con
	 * criterio distancia, utilizando el algoritmo de Kruskal.
	 */
	public void arbolMSTKruskalC1() {
		// TODO Auto-generated method stub

	}

	// TODO El tipo de retorno de los mï¿½todos puede ajustarse segï¿½n la
	// conveniencia
	/**
	 * Requerimiento 2C: Calcular un ï¿½rbol de expansiï¿½n mï¿½nima (MST) con
	 * criterio distancia, utilizando el algoritmo de Prim. (REQ 2C)
	 */
	public void arbolMSTPrimC2() {
		// TODO Auto-generated method stub

	}

	// TODO El tipo de retorno de los mï¿½todos puede ajustarse segï¿½n la
	// conveniencia
	/**
	 * Requerimiento 3C: Calcular los caminos de costo mï¿½nimo con criterio
	 * distancia que conecten los vï¿½rtices resultado de la aproximaciï¿½n de
	 * las ubicaciones de la cuadricula N x M encontrados en el punto 5.
	 */
	public void caminoCostoMinimoDijkstraC3() {
		// TODO Auto-generated method stub

	}

	// TODO El tipo de retorno de los mï¿½todos puede ajustarse segï¿½n la
	// conveniencia
	/**
	 * Requerimiento 4C:Encontrar el camino mï¿½s corto para un viaje entre dos
	 * ubicaciones geogrï¿½ficas escogidas aleatoriamente al interior del grafo.
	 * 
	 * @param idVertice2
	 * @param idVertice1
	 */
	public void caminoMasCortoC4(int idVertice1, int idVertice2) {
		// TODO Auto-generated method stub

	}

	public int darNumeroArcosOriginal() {
		return numeroDeArcosCola;
	}
	
	
	public void cargarTablas()
	{
		tablaIdNum = new LinearProbingHash<String, Integer>(grafo.V());
		tablaNumId = new LinearProbingHash<Integer, String>(grafo.V());
		
		RedBlackBalancedSearchTrees<String, InterseccionMallaVial> vertices = grafo.darVertices();
		Queue<String> ids = new Queue<String>();
		Iterator<String> iterator = vertices.keys().iterator();
		while(iterator.hasNext())
		{
			ids.enqueue(iterator.next());
		}
		for(int i = 0; i < grafo.V(); i++)
		{
			String id = ids.dequeue();
			tablaIdNum.put(id, i);
			tablaNumId.put(i, id);
		}
		
	}

}
