package vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {
	/**
	 * representa el ID
	 */
	private int objectID;
	private String Location;
	private double adressID;
	private int streetSEGID;
	private String xCOORD;
	private String yCOORD;
	private String ticketType;
	private int fineAMT;
	private double totalPaid;
	private int penalty1;
	private int penalty2;
	private String accidentIndicator;
	private String TicketIssueDate;
	private String violationCode;
	private String ViolationDesc;
	private String latitud;
	private String longitud;


	public VOMovingViolations(String pObjectID, String pLocation, String pAdressID, String pStreetSEGID, String pXCOORD,
			String pYCOORD, String pTicketType, String pFineAMT, String pTotalPaid, String pPenalty1, String pPenalty2,
			String pAccidentIndicator, String pTicketIssueDate, String pViolationCode, String pViolationDesc,
			String pLatitud, String plongitud) {
		objectID = Integer.parseInt(pObjectID);
		Location = pLocation;
		adressID = Double.parseDouble(pAdressID);
		streetSEGID = Integer.parseInt(pStreetSEGID);
		xCOORD = pXCOORD;
		yCOORD = pYCOORD;
		ticketType = pTicketType;
		fineAMT = Integer.parseInt(pFineAMT);
		totalPaid = Math.round((Double.parseDouble(pTotalPaid)));
		penalty1 = Integer.parseInt(pPenalty1);
		penalty2 = Integer.parseInt(pPenalty2);
		accidentIndicator = pAccidentIndicator;
		TicketIssueDate = pTicketIssueDate;
		violationCode = pViolationCode;
		ViolationDesc = pViolationDesc;
		latitud = pLatitud;
		longitud = plongitud;

	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		return objectID;
	}

	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		return Location;
	}

	/**
	 * @return adressID
	 */
	public double getAdressId() {
		return adressID;
	}

	public int getStreetSegid() {
		return streetSEGID;
	}

	public String getxCOORD() {
		return xCOORD;
	}

	public String getyCOORD() {
		return yCOORD;
	}

	/**
	 * @return ticketType .
	 */
	public String getTicketType() {
		return ticketType;
	}

	public int getFineAMT() {
		return fineAMT;
	}

	public double getTotalPaid() {
		return totalPaid;
	}

	public int getPenalty1() {
		return penalty1;
	}

	public int getPenalty2() {
		return penalty2;
	}

	public String gerAccidentIndicator() {
		return accidentIndicator;
	}

	public String getTicketIssueDate() {
		return TicketIssueDate;
	}

	public String getViolationCode() {
		return violationCode;
	}

	public String getViolationDesc() {
		return ViolationDesc;
	}

	public String getLatitud() {

		return latitud;
	}

	public String getLongitud() {
		return longitud;
	}
	
	public int compareTo(VOMovingViolations dato) {

		int RTA = 0;

		if (this.objectID < dato.objectID) {
			RTA = -1;
		} else if (this.objectID > dato.objectID) {
			RTA = 1;
		}
		return RTA;

	}

}
