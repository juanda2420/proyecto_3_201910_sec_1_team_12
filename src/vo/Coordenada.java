package vo;

public class Coordenada 
{
	double longitud;
	double latitud;
	
	public Coordenada(double pLongitud, double pLatitud)
	{
		longitud = pLongitud;
		latitud = pLatitud;
	}
	
	public double darLongitud()
	{
		return longitud;
	}
	
	public double darLatitud()
	{
		return latitud;
	}

}
