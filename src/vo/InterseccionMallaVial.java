package vo;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Representation of a Trip object
 */
public class InterseccionMallaVial implements Comparable<InterseccionMallaVial> {
	/**
	 * representa el ID
	 */
	private String objectID;
	private double latitud;
	private double longitud;
	private VOMovingViolations infraccion;
	private LinkedList<String> infracciones;
	private Double distancia_minima;
	private int cantidad_infracciones;
	private LinkedList<String> adyacentes;
	private ArrayList<arco_final> arcos_adyacentes = new ArrayList<arco_final>();

	public InterseccionMallaVial(String pObjectID, String pLatitud, String pLongitud) {
		objectID = pObjectID;
		latitud = Double.parseDouble(pLatitud);
		longitud = Double.parseDouble(pLongitud);
		distancia_minima = 0.0;
	}

	public InterseccionMallaVial(String pObjectID, String pLatitud, String pLongitud, LinkedList<String> infracciones,
			LinkedList<String> adyacentes) {
		objectID = pObjectID;
		latitud = Double.parseDouble(pLatitud);
		longitud = Double.parseDouble(pLongitud);
		this.infracciones = infracciones;
		cantidad_infracciones = infracciones.size();
		distancia_minima = 0.0;
		this.adyacentes = adyacentes;

	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public String objectId() {
		return objectID;
	}

	public double getLatitud() {
		return latitud;
	}

	public int getCantidadInfracciones() {
		return cantidad_infracciones;
	}

	public double getLongitud() {
		return longitud;
	}

	public LinkedList<String> darAdyacentes() {
		return adyacentes;
	}

	public ArrayList<arco_final> dar_arcos_adyacentes() {
		return arcos_adyacentes;
	}

	public void a�adir_arcos_adyacentes(arco_final e) {
		arcos_adyacentes.add(e);
	}

	public void setId(String id) {
		objectID = id;
	}

	public void setLongitud(double newLongitud) {
		longitud = newLongitud;
	}

	public void setLatitud(double newLatitud) {
		latitud = newLatitud;
	}

	public LinkedList<String> getInfracciones() {
		return infracciones;
	}

	public void setDistanciaMinima(Double dato) {
		distancia_minima = dato;
	}

	public Double darDistanciaMinima() {
		return distancia_minima;
	}

	public void setInfraccion(VOMovingViolations pInfraccion) {
		this.infraccion = pInfraccion;
	}

	public VOMovingViolations darInfraccion() {
		return infraccion;
	}

	public int compareTo(InterseccionMallaVial dato) {

		int RTA = 0;

		if (this.objectID.compareTo(dato.objectID) < 0) {
			RTA = -1;
		} else if (this.objectID.compareTo(dato.objectID) > 0) {
			RTA = 1;
		}
		return RTA;

	}

	public String toString() {
		return ("Identificador : " + objectID + "; Latitud : " + latitud + "; Longitud : " + longitud);
	}

}
