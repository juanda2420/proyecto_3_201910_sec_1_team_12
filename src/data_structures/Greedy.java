package data_structures;

import java.util.ArrayList;
import java.util.Iterator;

import vo.InterseccionMallaVial;
import vo.arco_final;

public class Greedy {

	public ArrayList<InterseccionMallaVial> Greedy(String v1, String v2, Graph grafo) {
		
		InterseccionMallaVial actual = grafo.darVertices().get(v1);
		
		InterseccionMallaVial destino = grafo.darVertices().get(v2);
		

		ArrayList<InterseccionMallaVial> cola = new ArrayList<InterseccionMallaVial>();
		
		ArrayList<Long> marcados = new ArrayList<Long>();
		
		Iterator<String> it = grafo.darVertices().keys().iterator();
		
		

		int contador = 0;
		
		while (actual != destino)
		{
			contador++ ;
			arco_final menor = null;
			ArrayList<arco_final> adyacentes = actual.dar_arcos_adyacentes();
			double distancia= 1000;
			
			for (arco_final arco : adyacentes) {
				 if ((arco.getCantidadInfracciones() <= distancia  && !marcados.contains(arco.darVertice2().objectId())))
				 {
					 menor = arco;
				 }
				 
				 if ( arco.darVertice2().objectId() == v2 && arco.getCantidadInfracciones()<= distancia  && !marcados.contains(arco.darVertice2().objectId())) {
					 menor = arco;
					 System.out.println("Entre");
					 break;
				 }
			}
			if (menor!=null) {
				marcados.add(Long.parseLong(menor.darVertice2().objectId()));
				actual = menor.darVertice2();
				cola.add(actual);
			}
		}
		
		for (InterseccionMallaVial vertice : cola) {
			System.out.println(vertice.objectId());
		}
		return cola;
	}
}