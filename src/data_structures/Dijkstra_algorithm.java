package data_structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import vo.Arcos_Calles;
import vo.InterseccionMallaVial;
import vo.arco_final;

public class Dijkstra_algorithm {

    private final RedBlackBalancedSearchTrees<String,InterseccionMallaVial> vertices;
    private final RedBlackBalancedSearchTrees<String,arco_final> arcos;
    private Set<InterseccionMallaVial> settledNodes;
    private Set<InterseccionMallaVial> unSettledNodes;
    private Map<InterseccionMallaVial, InterseccionMallaVial> predecessors;
    private Map<InterseccionMallaVial, Integer> distance;

    public Dijkstra_algorithm (RedBlackBalancedSearchTrees<String, InterseccionMallaVial> vertices, RedBlackBalancedSearchTrees<String, arco_final> arcos) {
        this.vertices = vertices;
        this.arcos = arcos;
    }

    public void execute(InterseccionMallaVial source) {
        settledNodes = new HashSet<InterseccionMallaVial>();
        unSettledNodes = new HashSet<InterseccionMallaVial>();
        distance = new HashMap<InterseccionMallaVial, Integer>();
        predecessors = new HashMap<InterseccionMallaVial, InterseccionMallaVial>();
        distance.put(source, 0);
        unSettledNodes.add(source);
        while (unSettledNodes.size() > 0) {
        	InterseccionMallaVial node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    private void findMinimalDistances(InterseccionMallaVial node) {
        List<InterseccionMallaVial> adjacentNodes = getNeighbors(node);
        for (InterseccionMallaVial target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }

    }

    private int getDistance(InterseccionMallaVial node, InterseccionMallaVial target) {
       
    	int total_infracciones = node.getCantidadInfracciones() + target.getCantidadInfracciones();
    	
    	return total_infracciones;
    	
    }

    private List<InterseccionMallaVial> getNeighbors(InterseccionMallaVial node) {
        
    	LinkedList<String> adyacentes_actual =  node.darAdyacentes();
    	
    	List<InterseccionMallaVial> arreglo = new ArrayList<InterseccionMallaVial>();
    	
    	for (int i = 0; i < adyacentes_actual.size(); i++) {
			
    		String id_actual = adyacentes_actual.get(i);
    		
    		arreglo.add(vertices.get(id_actual));
    			
		}
    	
    	return arreglo;
    	
    }

    private InterseccionMallaVial getMinimum(Set<InterseccionMallaVial> vertexes) {
    	InterseccionMallaVial minimum = null;
        for (InterseccionMallaVial vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }
        return minimum;
    }

    private boolean isSettled(InterseccionMallaVial vertex) {
        return settledNodes.contains(vertex);
    }

    private int getShortestDistance(InterseccionMallaVial destination) {
        Integer d = distance.get(destination);
        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }

    /*
     * This method returns the path from the source to the selected target and
     * NULL if no path exists
     */
    public LinkedList<InterseccionMallaVial> getPath(InterseccionMallaVial target) {
        LinkedList<InterseccionMallaVial> path = new LinkedList<InterseccionMallaVial>();
        InterseccionMallaVial step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }

}
